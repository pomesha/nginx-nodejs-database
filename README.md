# nginx + nodejs, database?

Это любительская настройка окружения для тренировок на "котятах".
Окружение включает в себя:  
а) node:14-alpine  
б) nginx:alpine  
?) нужно добавить DataBase (наверно, это будет - mySQL)  

## Getting started

1. Установка необходимого приложения, для этого нужно запустить отдельно "service: node", из состава docker-compose.yml, в интерактивном режиме:

	$ docker  run -it --rm \
	-u 1000:1000 \
	-w /home/node \
	-v $(pwd):/home/node \
	-p 1337:1337 \
	node:14-alpine \
	/bin/sh  

	$ cd backend && yarn  
	$ cd ../frontend && yarn

2. Устанавливаем backend/frontend


## Дополнения

смотреть логи контейнера после запуска docker-compose:  
$ docker-compose logs -f <name_container> 

```
cd existing_repo
git remote add origin https://gitlab.com/pomesha/nginx-nodejs-database.git
git branch -M main
git push -uf origin main
```
